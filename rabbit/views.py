#
# rabbit - serverpart for froxy
# 
# v0.0.1 - 2013-03-27
#


import sys, yaml, getpass, hashlib

from rabbit import app, request

hot = 0



@app.before_request
def check_hot():
    fx = app.config["RABBIT_FILE"]
    global hot
    ht = hot
    if ht == 0:
        hosts_populate(fx)

@app.route('/')
def index():
    api_key = app.config["API_KEY"]
    
    
    show = """

api_key:    %s

frost:      %s
    
    """ % (api_key, hdict)
    return show



@app.route('/frapi/<path:version>/<path:action>/', methods=["POST"])
def frapi(version=0, action=0, what=0, rapi_key=0):
    context="default"
    api_key = app.config["API_KEY"]
    if version == 0 or action == 0:
        return(redirect("/invalid/api"))
    
    if action !=  ("show"):
        return(redirect("/invalid/api"))
        

    
    if request.method == 'POST':
        try:
            rapi_key    = request.form['api_key'].strip()
            target     = request.form['target'].strip()
        except:
            return(redirect("/invalid/request"))
    
    else:
        return(redirect("/invalid/request"))

    
    if api_key.strip() == rapi_key.strip():
        print "HOORAY; key_match!"
    
    else:
        return(redirect("/invalid/apikey"))

    hosts = {}
    if target == "all":
        hosts = hdict
    else:
        try:
            hosts[target] = hdict[target]
        except:
            hosts[target] = hdict["s:%s" % target]
    res = yaml.dump(hosts)

        
    return(res)



def hosts_populate(fx, context="default"):
    chash = hashlib.sha512(context).hexdigest()
    global hdict, hot

    single = 1
    work_user = getpass.getuser()
    ssh_port = 22
    hdict = {} 


    try:
        finfo = yaml.load(file('%s' % fx, 'r'))
    except:
        print("error in loading frost_file %s" % fx) 
        hdict = ""
        return("error")


    try:
        finfo["login"]
        rss = finfo["login"]
        for xo in rss:
            print xo
            for x in xo:
                print x, xo[x]
                if x == "user":
                    work_user = xo[x]
                    print("setting work_user: %s" %  work_user)
                    continue
                elif x == "port":
                    ssh_port = xo[x]
                    print("setting work_port: %s" %  ssh_port)
                    continue


    except:
        pass

    for res in finfo:
        if res == "login":
            continue
        else:
            gname = res
            gval = finfo[res]
            if gval[0] == None:
                print("[i] empty group???: %s" %  gname)
                continue
            gout = []
            
            for x in gval:
                if len(x.split(":")) > 1:
                    myhost, myport = x.split(":")
                else:
                    myport = ssh_port
                    myhost = x

                try:
                    int(myport)
                except:
                    myport = ssh_port
                aval = "%s@%s:%s" % (work_user, myhost, myport)
                gout.append(aval)
                
                sid = "s:%s" % myhost
                if sid in hdict:
                    mnew = hdict[sid]
                    mnew.append(aval)
                    hdict[sid] = mnew
                else:
                    hdict[sid] = [aval]
                if "all" in hdict:
                    xnew = hdict["all"]
                    xnew.append(aval)
                    hdict["all"] = xnew 

                else:
                    hdict["all"] = [aval] 
            
            print """

Group   : %s
Members : %s
            """ % (gname, gval) 
        
            hdict[gname] = gout
        

    hot = 1
