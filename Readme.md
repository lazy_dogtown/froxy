

# FROXY - fab rabbit proxy - host/group-server and loadable module

froxy is a set of tools (server, client, library) to 

- maintain an external host/group-definitions in yaml-files 
- access these hosts/groups through a REST-API
- include a centralized access-directory into fabric-modules
- have a simple interface for displaying and editing
your host/group - definitions, no more env.hosts / env.roledefs 
anymore


this should provide a better way to maintain users/servers and groups
in an bigger environment. 

## Credits

This software has been created and Open-Source'd through sponsorship 
of Ergovia GmbH, Kiel, 

## Requirements

- python 2.5 or above 
- python virtualenv
    - flask 0.9.x
    - pyyaml
    - fabric
    - sqlite3

## 3 parts: Server, Interface, py-module


# Installation

we recommend to install python-virtualenv and then run

    ./install.sh -y
    
otherwise you can run install.sh in interactive-mode, this might
check for needed tools (see Requirements)

    ./install.sh -i



# Usage

froxy comes in 4 different "flavors", depending on your actions
(see froxy -h)


- work-mode 

    - work with froxy
    - listings
    - interactive interaction
    - cusotm commands on servers

- batch-mode    

    - t o be executed via cron/alias etc
    - custom execs
    - needs fab-files

- server-mode   

    - operate / maintain your central host/group - directory
    - access from your network

                   
- lib-mode      
    - include froxy_actions into your fab_files
    - acess all functions like host/groups - listings
    and command_execution from your fabric - receipts




## Work-mode / interactive actions and sessions




## batch-mode



     
## Server-Mode Rabbit-Server demon




    



## Lib-Mode

beside server and console-interface you're able to include
the functionalities into fabric - files, enabling you to use
a central data-store for your hosts without the need
to define your servers/groups everywhere


    from fabric.contrib.froxy_lib import *
    
    def rtest():
        run("hostname -f && uptime && uname -a") 
    
    
    @task
    def remote_test(group):
        host_list = return_hosts(group)    
        execute(rtest, hosts = host_list)


# Hosts -  lists (rabbit.list)

## section login (optional)

    login:
        - user: rabbit
        - port: 22

## section groups    

    
    group1:
        - server1
        - server2
        - g1server3:2222
    
    web:


# API 

## Requests:

- method: POST
- url: host:port/frapi/1/<action>/
    - action:
        - show - show a list of entries as dict
- data:
    - api_key: -> api_key
        - show -> list entries
    - target 
        - groupname || all -> list all members of this group


## answers

- returns as yaml-parseable text
- create a dictionary in python:
- if target was a single group, only this group will be included
in the answer

    res = yaml.load(answer)


# Usecase 


- 70+ Server
    - 40 Frontends
    - 12 DB
    - 20 Backend/BI/Support

- all running puppet for a defined configuration of basic packages 
and setup (acces, time, custom packages, monitoring, dc-integration etc)

- the usual "glue" of bash/python - scripts wasnt able to be managed anymore, 
although already designed with scripting-guidelines and a central tools.git

- 5 console-affine admins
- rundeck already in action
- needed an interactive, console-based deployment-tool with the following requirements:
    - free defineable tasks like
        - software-installation
        - customized java/tomcat - updates 
        - tasks like "please deploy version x.y.z onto staging-servers"
    - tasks on predefined groups or servers
    - simple and central accessible server/group - management
    - ideally python-based 
    

- evaluation:
    - fabric
    - puppet (nogo)
    - rundeck
    
- selected fabric:
    - python :)
    - very lightweight and easy
    - venv-able
    - extendeable

- usage
    - froxy/rabbit get filled automatically from a central database and resists on an own server
    
    - toolsv42.git distributes all sets of tools
        - deploy.py 
        - manage.py 
        - mist.py 
    
    - froxy defines jobs and hosts/groups, no need to grep for keywords
    
    - reenginnered the whole set of build_&_deploy_scripts (roughly 76.000 lines of
        spaghetti-code) within one day and rebuild that into 1 fab_file with 200 LOC
        

# License

- see License.txt
