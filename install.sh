#!/bin/bash


virtualenv --distribute venv
. venv/bin/activate

# if you dont use


easy_install Flask
easy_install memcache
easy_install mondodb
easy_install fabric
easy_install yaml

cp data/rabbit.list.template data/rabbit.list

mkdir -p data/fabs
